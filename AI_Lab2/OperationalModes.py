import ALModule
from DataLoader import DataLoader
import copy

data_loader: DataLoader = None
verbose = False


def resolution_mode(clauses_path, verbose_arg):
    # Let's load some data for starters
    global data_loader
    data_loader = DataLoader.fromFilename(clauses_path)

    global verbose
    verbose = verbose_arg

    resolve()


def interactive_mode(clauses_path, verbose_arg):
    global data_loader
    data_loader = DataLoader.fromFilename(clauses_path, goal_last_line=False)

    global verbose
    if verbose_arg:
        verbose = verbose_arg
        printKnowledgeBase()

    while True:
        print()
        if verbose:
            ALModule.verbose = True
            print("Please enter your query")
            print(">>> ", end='')

        user_input = input().lower()
        handle_input(user_input)


def test_mode(clauses_path, commands_path, verbose_arg = False):
    # Construct data_loader first
    global data_loader
    data_loader = DataLoader.fromFilename(clauses_path, commands_path, False)

    global verbose
    verbose = verbose_arg
    
    for line in data_loader.commands:
        if verbose:
            print()
            print(f"Currently processing >> {line}")
            
        handle_input(line)


########################################################################################################################
#                                           HELPER METHODS                                                             #
########################################################################################################################

def handle_input(user_input: str):
    # Time to handle input
    if user_input == 'exit':
        if verbose:
            print("Bye bye")

        exit(0)
    elif user_input == 'print kb':
        printKnowledgeBase()
        return

    # Need to get command and carry it out
    command = user_input[-1]
    clause = user_input[0:-2]

    # Check which command was specified
    if command == '+':
        # Add to dh's clauses
        add_clause(clause)
    elif command == '-':
        del_clause(clause)
    elif command == '?':
        query_clause(clause)
    else:
        if verbose:
            print("Unrecognised command")

def query_clause(clause):
    global data_loader
    restore_dh:DataLoader = copy.deepcopy(data_loader)

    data_loader.goal = frozenset(clause.split(" v "))
    data_loader.negate_goals()
    resolve()

    # After resolution is complete, need to restore data_loader to original state
    data_loader = restore_dh

def add_clause(to_add_str:str):
    to_add = frozenset(to_add_str.split(" v "))
    data_loader.clauses.add(to_add)
    data_loader.orderKeyItem[to_add] = data_loader.counter
    data_loader.orderKeyNum[data_loader.counter] = to_add
    data_loader.counter += 1

    if verbose:
        print("Added " + to_add_str)

def del_clause(to_del_str:str):
    to_del = frozenset(to_del_str.split(" v "))

    if to_del not in data_loader.clauses:
        # Nothing to delete ehh!
        if verbose:
            print(f"Not removing -> {to_del_str} isn't present in the knowledge base!")
        return

    data_loader.clauses.remove(to_del)
    to_del_counter = data_loader.orderKeyItem[to_del]
    del data_loader.orderKeyItem[to_del]
    del data_loader.orderKeyNum[to_del_counter]

    if verbose:
        print(f"Removed {to_del_str}")


def printKnowledgeBase():
    print("Resolution system constructed with knowledge:")
    for clause in data_loader.orderKeyNum.values():
        print(f"> {ALModule.pretty(clause)}")

def resolve():
    if verbose:
        data_loader.printClauses()
        ALModule.verbose = True

    # Time to check subsumation and consistency of the starting clauses!
    ALModule.checkSubsumedDataLoader(data_loader)
    res = ALModule.checkConsistency(data_loader.clauses, data_loader)
    if not res:
        # Invalid premises will derive any conclusion, including the goal
        if verbose:
            print("The premise clauses themselves are inconsistent, therefore deriving any conclusion!")

        data_loader.outcome = True
        data_loader.printOutcome()
        exit(0)

    status = ALModule.derive(data_loader)
    data_loader.outcome = status
    if verbose:
        data_loader.printProgress()

    # MUST print the outcome!
    data_loader.printOutcome()