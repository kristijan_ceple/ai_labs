import glob, os
import time

# Let's test resolution first
pythonRes = "python solution.py resolution "

start_time = time.time()
for filename in glob.glob('resolution_examples/*.txt'):
    toExec = pythonRes + filename
    print("> " + toExec.replace("\\", "/"))
    os.system(toExec)
    print()

print("Execution time: " + str(time.time() - start_time))