import sys, os
import subprocess
import time

javaCommand = "java -cp ./target/classes ui.Solution "
pythonCommand = "python solution.py "
cppCommand = "make && ./solution resolution resolution_examples/small_example.txt "
executingCommand = None
argument = sys.argv[1]

if argument == "java":
    executingCommand = javaCommand
elif argument == "python":
    executingCommand = pythonCommand
elif argument == "cpp":
    executingCommand = cppCommand
else:
    print("Illegal argument: [java/python/cpp]")
    exit(-1)

tasksToExecute = (
    'resolution resolution_examples/custom_1.txt',
    'resolution resolution_examples/custom_2.txt',
    'resolution resolution_examples/custom_3.txt',
    'resolution resolution_examples/custom_4.txt',
    'resolution resolution_examples/custom_5.txt',
    'resolution resolution_examples/custom_6.txt',
    'resolution resolution_examples/custom_7.txt',
    'resolution resolution_examples/custom_8.txt',
    'resolution resolution_examples/custom_9.txt',
    'resolution resolution_examples/custom_10.txt',
    'resolution resolution_examples/custom_11.txt',
    'resolution resolution_examples/ai.txt',
    'resolution resolution_examples/chicken_alfredo.txt',
    'resolution resolution_examples/chicken_alfredo_nomilk.txt',
    'resolution resolution_examples/chicken_broccoli_alfredo_big.txt',
    'resolution resolution_examples/coffee.txt',
    'resolution resolution_examples/coffee_noheater.txt',
    'resolution resolution_examples/small_example.txt',
    'resolution resolution_examples/small_example_2.txt',
    'resolution resolution_examples/small_example_3.txt',
    'resolution resolution_examples/small_example_4.txt',
    'resolution resolution_examples/coffe_or_tea.txt',
    'resolution resolution_examples/coffe_or_tea_nopowder.txt',
    'cooking_test cooking_examples/chicken_alfredo.txt cooking_examples/chicken_alfredo_input.txt',
    'cooking_test cooking_examples/chicken_alfredo_nomilk.txt cooking_examples/chicken_alfredo_nomilk_input.txt',
    'cooking_test cooking_examples/chicken_broccoli_alfredo_big.txt cooking_examples/chicken_broccoli_alfredo_big_input.txt',
    'cooking_test cooking_examples/coffee.txt cooking_examples/coffee_input.txt'
)

expectedOutput = (
    './resolution_examples/output/custom_1.txt',
    './resolution_examples/output/custom_2.txt',
    './resolution_examples/output/custom_3.txt',
    './resolution_examples/output/custom_4.txt',
    './resolution_examples/output/custom_5.txt',
    './resolution_examples/output/custom_6.txt',
    './resolution_examples/output/custom_7.txt',
    './resolution_examples/output/custom_8.txt',
    './resolution_examples/output/custom_9.txt',
    './resolution_examples/output/custom_10.txt',
    './resolution_examples/output/custom_11.txt',
    './resolution_examples/output/ai.txt',
    './resolution_examples/output/chicken_alfredo.txt',
    './resolution_examples/output/chicken_alfredo_nomilk.txt',
    './resolution_examples/output/chicken_broccoli_alfredo_big.txt',
    './resolution_examples/output/coffee.txt',
    './resolution_examples/output/coffee_noheater.txt',
    './resolution_examples/output/small_example.txt',
    './resolution_examples/output/small_example_2.txt',
    './resolution_examples/output/small_example_3.txt',
    './resolution_examples/output/small_example_4.txt',
    './resolution_examples/output/coffe_or_tea.txt',
    './resolution_examples/output/coffe_or_tea_nopowder.txt',
    './cooking_examples/output/chicken_alfredo.txt',
    './cooking_examples/output/chicken_alfredo_nomilk.txt',
    './cooking_examples/output/chicken_broccoli_alfredo_big.txt',
    './cooking_examples/output/coffee.txt',
)

def readFile(file_path) -> []:
    file = open(file_path, "r")
    toRet = []

    for liny in file:
        liny = liny.strip().lower()
        toRet.append(split_line(liny))

    return toRet

def split_line(to_split: str):
    # Wanna cut out the first part(without is true or is unknown)
    prefix = None
    suffix = None
    if to_split.endswith("is true"):
        suffix = "is true"
        prefix = to_split.replace("is true", "").strip()
    else:
        suffix = "is unknown"
        prefix = to_split.replace("is unknown", "").strip()

    toAdd = (frozenset(prefix.split(" v ")), suffix)
    return toAdd

i = 0
failed = 0
ok = 0

total_start = time.time()
for taskToExecute in tasksToExecute:
    print()

    line = executingCommand + taskToExecute

    task_start = time.time()
    result = subprocess.run(line, stdout=subprocess.PIPE)
    task_end = time.time()

    task_exec = (task_end - task_start)
    print(f"Task execution time: {task_exec}s")
    if(task_exec >= 20):
        print("!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!! >= 20 SEC")

    tempOutput = result.stdout.decode('utf-8').splitlines()
    correct_list = readFile(expectedOutput[i])

    to_check_list = []
    for liny in tempOutput:
        to_add = split_line(liny)
        to_check_list.append(to_add)

    if to_check_list != correct_list:
        print(f"{taskToExecute}\tFAILED!")
        failed += 1
        print("Expect output:")
        print('\n'.join(map(str, correct_list)))
        print("--------------")
        print("Actual output:")
        print('\n'.join(map(str, tempOutput)))
    else:
        ok += 1
        print(f"{taskToExecute}\tOK!")

    i += 1

total_end = time.time()
total_time = (total_end - total_start)
print()
print(f"Failed: {failed}")
print(f"OK: {ok}")
print(f"Percentage: {ok/(ok + failed) * 100}%")


print(f"Execution time: {total_time}")
if total_time >= 120:
    print("!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!! >= 120 SEC")
