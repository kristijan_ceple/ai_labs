import ALModule

class DataLoader:
    """
    s0 - contains initial state
    goalStates - the set of goal states
    trans - the transitions dictionary
    """

    def __init__(self):
        self.clauses = set()
        self.commands = []
        self.sos = set()
        self.goal = set()
        self.negGoals = set()
        self.orderKeyNum = {}
        self.orderKeyItem = {}

        self.clausesLoaded = False
        self.commandsLoaded = False
        self.dataLoaded = False

        self.counter = 1
        self.print_counter = 1
        self.progress = []
        self.outcome = False

    @classmethod
    def fromFilename(cls, clausesFile, commandsFile=None, goal_last_line=True):
        dh = cls()

        dh.loadClauses(clausesFile, goal_last_line)
        if not dh.clausesLoaded:
            raise AttributeError("Either commands or clauses not loaded correctly!")

        if commandsFile is not None:
            dh.loadCommands(commandsFile)

        return dh

    def loadClauses(self, filename, goal_last_line):
        # Let's open the file by the name filename
        file = open(filename, "r", encoding="utf8")

        # 1 line = 1 clause
        last = None
        for line in file:
            if line.startswith("#") or len(line) == 0:
                # This line contains a comment, or is empty
                continue

            # Lowercase the line
            line = line.lower().strip()
            parts = line.split(" v ")
            last = frozenset(parts)
            self.clauses.add(last)
            self.orderKeyItem[last] = self.counter
            self.orderKeyNum[self.counter] = last
            self.counter += 1

        if not goal_last_line:
            self.clausesLoaded = True
            return

        # Set the goal clause, and in the list negate the clause
        self.counter -= 1
        del self.orderKeyItem[last]
        del self.orderKeyNum[self.counter]

        self.goal = last
        self.clauses.remove(self.goal)
        self.negate_goals()

        self.clausesLoaded = True

    def negate_goals(self):
        # Now, take the goal clause, split by v operator, and then negate each atom
        # Add them to the SoS
        for goalPart in self.goal:
            toAppend = ALModule.involution(f"~{goalPart.strip()}")
            self.negGoals.add(frozenset([toAppend]))
            self.sos.add(frozenset([toAppend]))

            self.orderKeyNum[self.counter] = frozenset([toAppend])
            self.orderKeyItem[frozenset([toAppend])] = self.counter
            self.counter += 1

    def loadCommands(self, filename):
        file = open(filename, "r", encoding="utf8")

        for line in file:
            if line.startswith("#") or len(line) == 0:
                continue       # Either empty or comment

            line = line.lower().strip()
            self.commands.append(line)

        self.commandsLoaded = True


    def printClauses(self):
        # Print
        n = len(self.clauses)
        for num in range(1, n+1):
            entry = self.orderKeyNum.get(num)

            if entry is None:
                continue        # Gaps that come up due to adding/deleting

            toPrint = ' v '.join(entry)
            print ("{}. {}".format(self.print_counter, toPrint))
            self.print_counter += 1

        print("=============")
        # Time to print negated goal clauses
        m = len(self.orderKeyNum)
        for num in range(n+1, m+1):
            entry = self.orderKeyNum[num]
            toPrint = ' v '.join(entry)
            print("{}. {}".format(self.print_counter, toPrint))
            self.print_counter += 1
        print("=============")
        self.print_counter = self.print_counter

    def printProgress(self):
        for line in self.progress:
            if line.startswith("!"):
                print(f"{line}")
                continue

            print (f"{self.print_counter}. {line}")
            self.print_counter += 1

        print("=============")

    def printOutcome(self):
        to_print = "{}".format(' v '.join(self.goal))

        if self.outcome:
            print(f"{to_print} is true")
        else:
            print(f"{to_print} is unknown")

    def updateDictOrder(self, clause_to_add:frozenset):
        self.orderKeyItem[clause_to_add] = self.counter
        self.orderKeyNum[self.counter] = clause_to_add
        self.counter += 1