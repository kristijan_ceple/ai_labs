from collections import Iterable
import copy

verbose = False

def involution(literal:str):
    if type(literal) != str:
        raise TypeError("Argument to involution must be a string!")
    elif len(literal) == 0:
        raise ValueError("Empty string passed!")

    #Count the number of ~, and see if the number is odd or even
    negSum = 0
    for index, char in enumerate(literal):
        if char == '~':
            negSum += 1

    copyLiteral = literal[negSum:]
    if negSum % 2 == 1:
        return f"~{copyLiteral}"
    else:
        return copyLiteral

def pretty(container):
    if not isinstance(container, Iterable):
        return container

    to_print = "{}".format(' v '.join(map(str, container)))
    return to_print

def negate(literal:str) -> str:
    return involution(f"~{literal}")

def resolve(first:frozenset, second:frozenset):
    # First, find a pair of complementary literals which to resolve on
    found_literal = None
    negated_found = None
    for literal in first:
        negatedLiteral = negate(literal)
        if negatedLiteral in second:
            found_literal = literal
            negated_found = negatedLiteral
            break

    if found_literal is None:
        return None

    toRet = set(first.union(second))
    toRet.remove(found_literal)
    toRet.remove(negated_found)

    if checkValidity(toRet):
        return False        # Tautology

    return frozenset(toRet)


def checkMultipleValidity(clause:frozenset):
    """
        Checks validity of a single clause, but counts >= 2 occurrences
        :param clause: SE
        :return: whether it's valid or not, with at least 2(or more) occurrences
        """

    complSum = 0
    for literal in clause:
        negLiteral = involution(f"~{literal}")
        if negLiteral in clause:
            complSum += 1
            if complSum >= 3:
                return True

    return False  # No complementary literals

def checkValidity(clause: frozenset):
    """
    Checks validity of a single clause, counts up to 1 occurrence
    :param clause: SE
    :return: whether it's valid or not with 1 or more occurrences
    """
    # neg_clause = set()
    # for literal in clause:
    #     negLiteral = involution(f"~{literal}")
    #     neg_clause.append(negLiteral)

    for literal in clause:
        negLiteral = involution(f"~{literal}")
        if negLiteral in clause:
            return True

    return False        # No complementary literals


def checkSubsumedClausePair(clause:frozenset, other_clause:frozenset):
    """
    Checks whether clauses subsume one (or) another
    :param clause: first arg
    :param other_clause: second arg
    :return: The clause to be deleted, or false if no deletion should take place
    """

    # Now compare element by element
    if other_clause.issubset(clause):
        return clause
    elif clause.issubset(other_clause):
        return other_clause
    else:
        return False


def checkSubsumedWhile(to_check:frozenset, dh:'DataLoader'):
    # Check if the current clause subsumes any other clause in the dh
    # Or whether it is subsumed by any other clause

    # First check the sos
    to_delete = set()
    for sos_clause in dh.sos:
        if to_check is sos_clause:
            continue

        if to_check.issubset(sos_clause):
            to_delete.add(sos_clause)
            # Add to progress
            dh.progress.append(f"!SUBSUMPTION: {pretty(to_check)} <= {pretty(sos_clause)}")

    dh.sos.difference_update(to_delete)
    to_delete.clear()

    for clause in dh.clauses:
        if to_check.issubset(clause):
            to_delete.add(clause)
            # Add to progress
            dh.progress.append(f"!SUBSUMPTION: {pretty(to_check)} <= {pretty(clause)}")

    dh.clauses.difference_update(to_delete)


def checkSubsumedDataLoader(dh:'DataLoader'):
    # At the beginning, have to go through all the clauses
    # Means - check the SoS set and clauses set for subsumation, and delete corresponding clauses if necessary
    # In SoS is only located the negated goal
    changed = True
    while changed:
        changed = False

        for sos_clause in dh.sos:
            for other_clause in dh.clauses:
                res = checkSubsumedClausePair(sos_clause, other_clause)
                if not res:
                    continue            # Nothing to do here, no subsumation
                elif res is sos_clause:
                    dh.sos.remove(sos_clause)
                    dh.progress.append(f"!START_OF_LIFE SUBSUMPTION: {pretty(other_clause)} <= {pretty(sos_clause)}")
                    changed = True
                    break
                else:
                    # Res must be other_clause!
                    dh.clauses.remove(other_clause)
                    dh.progress.append(f"!START_OF_LIFE SUBSUMPTION: {pretty(sos_clause)} <= {pretty(other_clause)}")
                    changed = True
                    break
            else:
                continue

            break

        # Maybe we deleted something, maybe we didn't delete anything
        # If something was deleted, we need to give it another go either way
        if changed:
            continue

        # Now check other clauses in between themselves
        for first in dh.clauses:
            for second in dh.clauses:
                if first is second:
                    continue

                res = checkSubsumedClausePair(first, second)
                if not res:
                    continue
                else:
                    dh.clauses.remove(res)

                    stays = first
                    if res is first:
                        stays = second

                    dh.progress.append(f"!START_OF_LIFE SUBSUMPTION: {pretty(stays)} <= {pretty(res)}")
                    changed = True
                    break
            else:
                continue

            break

        if changed:
            continue


def checkSubsumed(clauses_set:set):
    # Check if all literals of 1 clause, are contained within another clause
    # Delete the other clause
    while True:
        to_delete = None        # Reset deleting!!!
        for clause in clauses_set:
            subsumed = False
            for other_clause in clauses_set:
                if clause is other_clause:
                    continue        # Skip same element

                # Now compare element by element
                if other_clause.issubset(clause):
                    subsumed = True
                    to_delete = clause
                    break
                elif clause.issubset(other_clause):
                    subsumed = True
                    to_delete = other_clause
                    break

                if subsumed:
                    break

            if subsumed:
                break       # Propagate the subsumation

        # Check if there's need to delete anyone!
        if to_delete is not None:
            clauses_set.remove(to_delete)
        else:
            break       # Nothing changed!



def checkConsistency(clauses_set:set, dh:'DataLoader' = None):
    """
    Checks for inconsistencies in the clauses_List
    :param clauses_list: self-explanatory
    :return: true if consistent, false if inconsistent
    """
    # Need to check if any clause is the negation of any other
    # To this effect, I will construct an inverse clauses frozenset
    neg_clauses = {}
    for clause in clauses_set:
        to_add = set()
        for literal in clause:
            to_add.add(negate(literal))
        neg_clauses[clause] = frozenset(to_add)

    # Now test!
    for clause in clauses_set:
        neg_clause = neg_clauses[clause]
        # Now check if it is equal to any other clause in the clauses_set
        res = clauses_set.isdisjoint(neg_clause)        # clauses_set must not have neg_clause
        if not res:
            if dh:
                dh.progress.append(f"!STARTING PREMISES INCONSISTENT!!! >> {pretty(clause)} and {pretty(neg_clause)}")
            return False            # NOT disjoint!

    return True     # Consistent!

def derive_transparent(dh: 'DataLoader') -> bool:
    # Back Up data first!
    throw_away_dh = copy.deepcopy()
    return derive(throw_away_dh)


def derive(dh: 'DataLoader') -> bool:
    # Let's begin by taking stuff from the SoS dict
    marked_pairs = {}
    while True:
        # Will have a symmetric dictionary in which I'll save pairs that have been done in the current iter
        changed = False
        for first in dh.sos:
            for second in dh.sos:
                if first is second:
                    continue        # Check if same clause

                # Not same clause
                if (marked_pairs.get(first) and marked_pairs.get(first).get(second)) \
                    or (marked_pairs.get(second) and marked_pairs.get(second).get(first)):
                    continue        # Already been done :/

                # Found them!
                res = resolve(first, second)
                marked_pairs.setdefault(first, {second: True})[second] = True
                marked_pairs.setdefault(second, {first: True})[first] = True

                if res is False:
                    # Tautology / Valid formula
                    dh.progress.append(f"!VALID {pretty(res)} ({pretty(dh.orderKeyItem[first])}, {pretty(dh.orderKeyItem[second])})")
                    continue
                elif res is None:
                    # Couldn't resolve
                    continue

                dh.updateDictOrder(res)
                dh.sos.add(res)                 # Add to SoS
                if len(res) == 0:
                    # The NIL clause! Success!
                    dh.progress.append(f"NIL ({pretty(dh.orderKeyItem[first])}, {pretty(dh.orderKeyItem[second])})")
                    return True
                else:
                    changed = True
                    dh.progress.append(f"{pretty(res)} ({pretty(dh.orderKeyItem[first])}, {pretty(dh.orderKeyItem[second])})")
                    checkSubsumedWhile(res, dh)
                    break
            else:
                continue
            break

        if changed:
            continue        # Some items were added to the SoS -- go at it again
                            # Either way, we wouldn't want to change the set we're iterating over

        # Just exited the loop -> Means (sos_el, sos_el) pair couldn't have been found
        for sos_clause in dh.sos:
            for other_clause in dh.clauses:
                # Not same clause
                if (marked_pairs.get(sos_clause) and marked_pairs.get(sos_clause).get(other_clause)) \
                        or (marked_pairs.get(other_clause) and marked_pairs.get(other_clause).get(sos_clause)):
                    continue  # Already been done :/

                # Not done!!! Let's try to resolve them
                res = resolve(sos_clause, other_clause)
                marked_pairs.setdefault(sos_clause, {other_clause: True})[other_clause] = True
                marked_pairs.setdefault(other_clause, {sos_clause: True})[sos_clause] = True

                if res is False:
                    # Tautology / Valid formula
                    dh.progress.append(f"!VALID {pretty(res)} ({pretty(dh.orderKeyItem[first])}, {pretty(dh.orderKeyItem[second])})")
                    continue
                elif res is None:
                    # Couldn't resolve
                    continue

                # Ohh nice we got some results!
                dh.updateDictOrder(res)
                dh.sos.add(res)         # Add to SoS
                if len(res) == 0:
                    # The NIL clause! Success!
                    dh.progress.append(f"NIL ({pretty(dh.orderKeyItem[sos_clause])}, {pretty(dh.orderKeyItem[other_clause])})")
                    return True
                else:
                    changed = True
                    dh.progress.append(f"{pretty(res)} ({pretty(dh.orderKeyItem[sos_clause])}, {pretty(dh.orderKeyItem[other_clause])})")
                    checkSubsumedWhile(res, dh)
                    break
            else:
                continue

            break

        if changed:
            continue        # Same case as above

        # Finally, we exited both loops, and there were no changes to the SoS set, which just couldn't find any candidates for resolution
        # Therefore, the algorithm hasn't been able to derive NIL
        return False
