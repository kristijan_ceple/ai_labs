import argparse
from OperationalModes import resolution_mode, test_mode, interactive_mode

parser = argparse.ArgumentParser()
parser.add_argument("assignment_flag", type=str,
                    choices = ["resolution", "cooking_test", "cooking_interactive"],
                    help = "Specify which assignment is being tested")
parser.add_argument("clauses_path", type=str,
                  help = "Specify the path to the clauses file")
parser.add_argument("commands_path", type=str, nargs='?',
                  help = "Specify the path to the commands file")
parser.add_argument("-v", "--verbose", action="store_true")
args = parser.parse_args()

if args.assignment_flag == "resolution":
    resolution_mode(args.clauses_path, args.verbose)
elif args.assignment_flag == "cooking_test":
    test_mode(args.clauses_path, args.commands_path, args.verbose)
else:
    interactive_mode(args.clauses_path, args.verbose)

