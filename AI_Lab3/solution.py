import argparse
import DataLoader
from ID3 import ID3
import operator

# Let's set up arg parse
parser = argparse.ArgumentParser()
parser.add_argument("train_path", type=str,
                    help="Path to the train dataset.")
parser.add_argument("test_path", type=str,
                    help="Path to the test dataset.")
parser.add_argument("config_path", type=str,
                    help="Path to the configuration file.")
args = parser.parse_args()

trainDataset = DataLoader.loadDatasetFile(args.train_path)
testDataset = DataLoader.loadDatasetFile(args.test_path)
config = DataLoader.loadConfigFile(args.config_path)

model = ID3(config)
test_output = model.fit(trainDataset)
predictions = model.predict(testDataset)

test_output.sort(key=operator.itemgetter(0, 1))
print(', '.join(':'.join(map(str, x)) for x in test_output))
print(' '.join(predictions))

acc, conf_matrix = model.summarise_performance(predictions, testDataset)
print(f"{acc:.5f}")
for row in conf_matrix:
    print(' '.join(map(str, row)))
print()