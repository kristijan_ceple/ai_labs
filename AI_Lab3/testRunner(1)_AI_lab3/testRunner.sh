#!/bin/bash

IFS=""

javaCommand="java -cp ./target/classes ui.Solution "
pythonCommand="python solution.py "
cppCommand="make && ./solution resolution resolution_examples/small_example.txt "


if [ $1 == 'java' ]
then
	executingCommand=$javaCommand
elif [ $1 == 'python' ]
then
	executingCommand=$pythonCommand
elif [ $1 == 'cpp' ]
then
	executingCommand=$cppCommand
else
	echo krivi argument:[java/python/cpp]
	exit
fi


tasksToExecute=(
				#test=train
				'datasets/volleyball.csv datasets/volleyball.csv config/id3.cfg'
				'datasets/logic_small.csv datasets/logic_small.csv config/id3.cfg'
				'datasets/titanic_train_categorical.csv datasets/titanic_train_categorical.csv config/id3.cfg'
				#regular
				'datasets/volleyball.csv datasets/volleyball_test.csv config/id3.cfg'
				'datasets/logic_small.csv datasets/logic_small_test.csv config/id3.cfg'
				'datasets/titanic_train_categorical.csv datasets/titanic_test_categorical.csv config/id3.cfg'

				'datasets/volleyball.csv datasets/volleyball_test.csv config/id3_maxd1.cfg'
				'datasets/logic_small.csv datasets/logic_small_test.csv config/id3_maxd1.cfg'
				'datasets/titanic_train_categorical.csv datasets/titanic_test_categorical.csv config/id3_maxd1.cfg'
				#custom
				'datasets/volleyball.csv datasets/volleyball_test_custom.csv config/id3.cfg'
				'datasets/logic_small.csv datasets/logic_small_test_custom.csv config/id3.cfg'
				'datasets/titanic_train_categorical.csv datasets/titanic_test_categorical_custom.csv config/id3.cfg'
				#pseudo rf
				#'datasets/volleyball.csv datasets/volleyball_test.csv config/rf_n5_sub05.cfg pseudorf_definitions/volleyball.txt'
				#'datasets/logic_small.csv datasets/logic_small_test.csv config/rf_n9_sub0705.cfg pseudorf_definitions/logic_small.txt'
				#'datasets/titanic_train_categorical.csv datasets/titanic_test_categorical.csv config/rf_n9_sub05.cfg pseudorf_definitions/titanic.txt'
				)
expectedOutput=(
				'./output/volleyball_id3_train_train.txt'
				'./output/logic_small_id3_train_train.txt'
				'./output/titanic_id3_train_train.txt'

				'./output/volleyball_id3.txt'
				'./output/logic_small_id3.txt'
				'./output/titanic_id3.txt'

				'./output/volleyball_id3_maxd1.txt'
				'./output/logic_small_id3_maxd1.txt'
				'./output/titanic_id3_maxd1.txt'

				'./output/custom_volleyball.txt'
				'./output/custom_logic.txt'
				'./output/custom_titanic.txt'

				#'./output/volleyball_rf_n5_sub05.txt'
				#'./output/logic_small_rf_n9_sub0705.txt'
				#'./output/titanic_rf_n9_sub05.txt'
				)
tempOutput='temp_output.txt'

i=0
failed=0
ok=0;

for taskToExecute in ${tasksToExecute[*]}
do
	touch $tempOutput
	line=$executingCommand$taskToExecute
	eval $line > $tempOutput
	
	if [ $(diff -w -B "$tempOutput" "${expectedOutput[$i]}") ] 
	then
		echo
		failed=$(($failed+1))
		echo $taskToExecute FAILED
		echo "expected output:"
		cat ${expectedOutput[$i]}
		echo
		echo "actually:"
		cat $tempOutput
		echo
	else
		ok=$(($ok+1))
		echo $taskToExecute ok
	fi
	
	rm $tempOutput
	i=$(($i+1))
done

echo
echo failed: $failed
echo ok:     $ok
echo
echo percentage: $(($ok*100/($ok+$failed)))%



