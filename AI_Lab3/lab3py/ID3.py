import math
from DataLoader import Dataset

EPSILON = 1E-6


class Node:
    test_mode_output_begin = True

    def __init__(self, D, is_leaf: bool, depth: int,
                 children: dict = {},
                 E: float = 0,
                 clb_cnts_list: list = [], clb_cnts_sum: int = 0,
                 ):
        self.depth = depth
        self.feature_index = None
        self.feature_name = None

        self.D = D           # The examples for this Node

        self.children = {}      # Pointers to children
        self.children.update(children)

        self.is_leaf = is_leaf

        # Now the extra parameters
        self.E = E
        self.clb_cnts_list = clb_cnts_list      # Todo: Check to maybe make this a copy-statement
        self.clb_cnts_sum = clb_cnts_sum

        # Check if E is 0, and if it is initialise the decisive class label
        self.clb_index = None
        if E <= EPSILON:
            # Oh yeah
            self.is_leaf = True
            for i in range(len(self.clb_cnts_list)):
                clb_cnt = clb_cnts_list[i]
                if clb_cnt > 0:
                    self.clb_index = i
                    break

    def set_clb_index(self, best_clb_index):
        self.is_leaf = True
        self.clb_index = best_clb_index


class ID3:

    def __init__(self, hyperpars: dict):
        self._initial_dataset_entropy = None
        self.trainDataset: Dataset = None

        self.max_depth = hyperpars.get('max_depth', None)
        if self.max_depth is not None:
            self.max_depth = int(self.max_depth)

        self.root = None
        self.fit_done = False

        # Update the test var for autograder
        test_mode_str = hyperpars['mode']
        if test_mode_str == 'test':
            self.test_mode = True
        else:
            self.test_mode = False

        # Update max depth
        if self.max_depth is not None and self.max_depth >= 1:
            self.max_depth_set = True
        else:
            self.max_depth_set = False

        self.test_output = []

    def fit(self, trainDataset: Dataset):
        # Clear the output first
        self.test_output.clear()

        self.trainDataset = trainDataset
        D = trainDataset.data
        # Okay, let's construct the root.
        self.root = self.process_root(D)

        # Check if the root already is a leaf!
        if self.root.is_leaf:
            # We're done!
            self.fit_done = True
            return self.test_output

        # Now go over all the children of this node
        for child in self.root.children.values():
            if child.is_leaf:
                continue

            # Else we got a non-leaf. Need to process that node as well
            features_indices_prime = list(self.trainDataset.features.values())
            features_indices_prime.pop(self.root.feature_index)
            
            self.process_node(features_indices_prime, child)

        # This is all well and fine, but now we wanna make it recursive
        # Let us think about that for a second. Let it siiink in
        # So, what we need to do is upgrade the other 2 funcs and do some begining steps here
        # Okay, all done. Now just set the flag to true
        self.fit_done = True
        return self.test_output

    def process_node(self, features_indices: list, to_process: Node):
        # First check if D is empty. If so, it's a leaf!
        if to_process.is_leaf or (self.max_depth_set and to_process.depth >= self.max_depth):
            if not self.test_mode:
                print(f"Leaf encountered at depth{to_process.depth} -> "
                      f"{self.trainDataset.class_labels_reverse[to_process.clb_index]}")
            return True     # No need to process leaves

        D = to_process.D
        # Check if D = empty
        if len(D) == 0:
            # It's a leaf then ehh
            return False    # The node above will do democracy on parent D

        if len(features_indices) == 0:
            best_clb_index = self.democracy(to_process)
            to_process.set_clb_index(best_clb_index)        # This node's done!
            return True

        # Gotta do the same as the process_root method, except we're working on an already present node
        # Okay, let's choose the root node, and add it to the tree. Will do recursion later
        # Go through all the features, and calculate IG for each feature for the initial dataset instance
        ######################################        DATA STRUCTS        ##############################################
        IGs = {}
        # dataset_entropy = self.entropy_dataset(D)

        feature_vals_entropies = {}
        feature_vals_clbs = {}
        feature_vals_clbs_sums = {}

        features_vals_processed = {}
        ######################################        DATA STRUCTS        ##############################################

        ####################################        DATA STRUCT INIT        ############################################
        for feature_index in features_indices:
            feature_vals_entropies[feature_index] = {}
            feature_vals_clbs[feature_index] = {}
            feature_vals_clbs_sums[feature_index] = {}

        class_labels_indices_template = []
        for i in range(len(self.trainDataset.class_labels)):
            class_labels_indices_template.append(0)  # Prepare the template -> for each class label!
        ####################################        DATA STRUCT INIT        ############################################
        for feature_index in features_indices:
            # Okay, now we've got the index of this feature, for each distinct value of the feature gotta calculate the
            # entropy
            feature_vals = {}  # Class labels
            feature_val_entropies = {}  # Self-explanatory
            feature_vals_processed = set()

            # Go through all the examples, and fill the feature_vals hash map with the number of class labels
            # Also fill the feature_clbs list(and update feature_clbs_sum)
            for example in D:
                feature_val = example[feature_index]
                class_label = self.trainDataset.class_labels[example[-1]]

                to_check = feature_vals.get(feature_val)
                if to_check is None:
                    feature_vals[feature_val] = class_labels_indices_template[:]

                feature_vals[feature_val][class_label] += 1  # Increment the frequency of the class label
                feature_vals_processed.add(feature_val)

            # Okay, let's calculate the IGs! For that, we will need the entropy of each feature value calculated!
            # Calculate the entropy of each feature_val!
            for feature_val, feature_val_clbs_cnt_list in feature_vals.items():
                clbs_cnt_sum = sum(feature_val_clbs_cnt_list)
                feature_vals_clbs_sums[feature_index][feature_val] = clbs_cnt_sum
                feature_val_entropies[feature_val] = ID3.entropy_feature_val(feature_val_clbs_cnt_list, clbs_cnt_sum)

            # Okay, time to get the IG on the road!
            IGs[feature_index] = ID3.IG_feature(to_process.E, feature_vals,
                                                feature_val_entropies)
            # All set -> proceed to choose the feature with the highest IG

            feature_vals_entropies[feature_index] = feature_val_entropies
            feature_vals_clbs[feature_index] = feature_vals
            features_vals_processed[feature_index] = feature_vals_processed

        # First sort them alphabetically, and then choose the best one
        IGs_alph_keys = list(map(self.trainDataset.features_reverse.get, list(IGs.keys())))
        IGs_alph_keys.sort()

        best_feature_index = -1  # No feature found
        best_feature_IG_val = -1  # No entropy gain as default
        
        for feature_name in IGs_alph_keys:
            feature_index = self.trainDataset.features[feature_name]
            feature_IG_val = IGs[feature_index]
            feature_name = self.trainDataset.features_reverse[feature_index]

            if feature_IG_val > best_feature_IG_val:
                best_feature_index = feature_index
                best_feature_IG_val = feature_IG_val

        # First check if we've run out of features
        if best_feature_index == -1:
            # Do democracy on the current example set
            best_clb_index = self.democracy(to_process)
            to_process.set_clb_index(best_clb_index)        # This node's done!
            return True

        # Okay, time to finally modify the node that we have been given
        to_process_feature_name = self.trainDataset.features_reverse[best_feature_index]
        to_process_feature_index = best_feature_index
        self.set_feature(to_process_feature_name, to_process_feature_index, to_process)

        # Control output
        if not self.test_mode:
            # Sort by IG gain
            IGs_gain = sorted(IGs.items(), key=lambda x: x[1], reverse=True)
            print(f"Parent feature: {to_process.feature_name} at Depth {to_process.depth}")

            was_in_loop = False
            for feature_index, feature_IG_val in IGs_gain:
                was_in_loop = True
                feature_name = self.trainDataset.features_reverse[feature_index]
                print(f"IG({feature_name})={feature_IG_val:.4f}", end=' ')

            if was_in_loop:
                print()         # Prints a newline

        init_children = to_process.children
        children_depth = to_process.depth + 1

        for feature_val in features_vals_processed[best_feature_index]:
            Dprime = self.calculate_Dprime(D, best_feature_index, feature_val)
            # For making children notes should cache data from above
            init_children[feature_val] = Node(
                Dprime, False, children_depth,
                E=feature_vals_entropies[best_feature_index][feature_val],
                clb_cnts_list=feature_vals_clbs[best_feature_index][feature_val],
                clb_cnts_sum=feature_vals_clbs_sums[best_feature_index][feature_val]
            )

        # About to go recursive boye!
        for feature_branch_value, child_node in init_children.items():
            features_indices_prime = features_indices[:]
            features_indices_prime.remove(best_feature_index)

            no_democracy = self.process_node(features_indices_prime, child_node)
            if no_democracy is False:
                # Edge case D' = empty. Gotta do democracy on the parent set
                best_clb_index = self.democracy(to_process)
                child_node.set_clb_index(best_clb_index)

        return True

    @staticmethod
    def calculate_Dprime(D: list, best_feature_index: int, feature_branch_value):
        Dprime = []
        for row in D:
            if row[best_feature_index] != feature_branch_value:
                continue
            else:
                row_copy = row[:]
                row_copy[best_feature_index] = None
                Dprime.append(row_copy)

        return Dprime

    def process_root(self, D: list):
        # Okay, let's choose the root node, and add it to the tree. Will do recursion later
        # Go through all the features, and calculate IG for each feature for the initial dataset instance
        ######################################        DATA STRUCTS        ##############################################
        IGs = {}
        # dataset_entropy = self.entropy_dataset(D)

        features_entropies = {}
        features_clbs = {}
        features_clbs_sums = {}

        feature_vals_entropies = {}
        feature_vals_clbs = {}
        feature_vals_clbs_sums = {}

        to_ret = None
        ######################################        DATA STRUCTS        ##############################################

        ####################################        DATA STRUCT INIT        ############################################
        for feature_index in self.trainDataset.features.values():
            feature_vals_entropies[feature_index] = {}
            feature_vals_clbs[feature_index] = {}
            feature_vals_clbs_sums[feature_index] = {}

        class_labels_indices_template = []
        for i in range(len(self.trainDataset.class_labels)):
            class_labels_indices_template.append(0)  # Prepare the template -> for each class label!
        ####################################        DATA STRUCT INIT        ############################################
        for feature_index in self.trainDataset.features.values():
            # Okay, now we've got the index of this feature, for each distinct value of the feature gotta calculate the
            # entropy
            feature_vals = {}  # Class label counting for each distinct value
            feature_val_entropies = {}  # Self-explanatory

            feature_clbs = class_labels_indices_template[:]  # Copy the template! This list is used to calc entropy!
            feature_clbs_sum = 0

            # Go through all the examples, and fill the feature_vals hash map with the number of class labels
            # Also fill the feature_clbs list(and update feature_clbs_sum)
            for example in D:
                feature_val = example[feature_index]
                class_label_index = self.trainDataset.class_labels[example[-1]]

                to_check = feature_vals.get(feature_val)
                if to_check is None:
                    feature_vals[feature_val] = class_labels_indices_template[:]  # Prepare the list

                feature_vals[feature_val][class_label_index] += 1  # Increment the frequency of the class label

                # Time to update the class labels list as well!
                feature_clbs[class_label_index] += 1
                feature_clbs_sum += 1

            # Okay, gone through all the examples for this feature, and for each and every feature_val have enumerated
            # the number of each class labels --> therefore need to calculate the entropy of this feature_val
            # Have got all I need to calculate the entropy  of this feature- -> need the number of class labels that
            # have appeared in this feature
            features_entropies[feature_index] = ID3.entropy_feature(feature_clbs, feature_clbs_sum)

            # Okay, let's calculate the IGs! For that, we will need the entropy of each feature value calculated!
            # Calculate the entropy of each feature_val!
            for feature_val, feature_val_clbs_cnt_list in feature_vals.items():
                clbs_cnt_sum = sum(feature_val_clbs_cnt_list)
                feature_vals_clbs_sums[feature_index][feature_val] = clbs_cnt_sum
                feature_val_entropies[feature_val] = ID3.entropy_feature_val(feature_val_clbs_cnt_list, clbs_cnt_sum)

            # Okay, time to get the IG on the road!
            IGs[feature_index] = ID3.IG_feature(features_entropies[feature_index], feature_vals, feature_val_entropies)
            # All set -> proceed to choose the feature with the highest IG

            # Before exiting, update data structs
            features_clbs[feature_index] = feature_clbs
            features_clbs_sums[feature_index] = feature_clbs_sum

            feature_vals_entropies[feature_index] = feature_val_entropies
            feature_vals_clbs[feature_index] = feature_vals

        # First sort them alphabetically, and then choose the best one
        IGs_alph_keys = list(map(self.trainDataset.features_reverse.get, list(IGs.keys())))
        IGs_alph_keys.sort()

        best_feature_index = -1  # No feature found
        best_feature_IG_val = -1  # No entropy gain as default
        for feature_name in IGs_alph_keys:
            feature_index = self.trainDataset.features[feature_name]
            feature_IG_val = IGs[feature_index]
            feature_name = self.trainDataset.features_reverse[feature_index]

            if feature_IG_val > best_feature_IG_val:
                best_feature_index = feature_index
                best_feature_IG_val = feature_IG_val

        # Okay, time to finally make the root node!
        # Dprime = D[:]
        # Dprime.pop(best_feature_index)
        to_ret = Node(
            D, False, 0,
            E=features_entropies[best_feature_index],
            clb_cnts_list=features_clbs[best_feature_index],
            clb_cnts_sum=features_clbs_sums[best_feature_index]
        )

        best_feature_name = self.trainDataset.features_reverse[best_feature_index]
        self.set_feature(best_feature_name, best_feature_index, to_ret)

        # Control output
        if not self.test_mode:
            # Sort by IG gain
            IGs_gain = sorted(IGs.items(), key=lambda x: x[1], reverse=True)
            print(f"Root feature: {to_ret.feature_name} at Depth 0")

            was_in_loop = False
            for feature_index, feature_IG_val in IGs_gain:
                was_in_loop = True
                feature_name = self.trainDataset.features_reverse[feature_index]
                print(f"IG({feature_name})={feature_IG_val:.4f}", end='  ')

            if was_in_loop:
                print()  # Prints a newline

        init_children = to_ret.children
        for feature_val in self.trainDataset.feature_index_2_vals[best_feature_index]:
            Dprime = self.calculate_Dprime(D, best_feature_index, feature_val)

            # For making children notes should cache data from above
            init_children[feature_val] = Node(
                Dprime, False, 1,
                E=feature_vals_entropies[best_feature_index][feature_val],
                clb_cnts_list=feature_vals_clbs[best_feature_index][feature_val],
                clb_cnts_sum=feature_vals_clbs_sums[best_feature_index][feature_val]
            )

        return to_ret

    @staticmethod
    def IG_feature(feature_entropy: float, feature_vals: dict, feature_val_entropies: dict):
        # Have to do a weighted sum!
        sigma = feature_entropy

        elems_cnt = 0
        for feature_val_clb_cnts in feature_vals.values():
            elems_cnt += sum(feature_val_clb_cnts)

        for feature_val, feature_val_entropy in feature_val_entropies.items():
            feature_val_clb_cnts = feature_vals[feature_val]

            # Entropy of feature val * its weight, sum them up into this var
            sigma -= feature_val_entropy * sum(feature_val_clb_cnts) / elems_cnt

        return sigma

    def predict(self, test_dataset: Dataset):
        # Check if fit has been done
        if not self.fit_done:
            raise AttributeError("The model hasn't been trained! Therefore no testing can be done!")

        # Okay, let's go row by row and do this
        output = []
        row_counter = 0
        for row in test_dataset.data:
            # Okay, take the tree now
            curr_node: Node = self.root
            row_counter += 1
            while True:
                curr_feature_index = curr_node.feature_index

                if curr_feature_index is None:
                    # Democracy!
                    best_clb_index = self.democracy(curr_node)  # Time for some democracy
                    # After we've done, just append the best one
                    output.append(self.trainDataset.class_labels_reverse[best_clb_index])
                    break

                curr_feature_val = row[curr_feature_index]

                # Get the corresponding child now!
                next_node: Node = curr_node.children.get(curr_feature_val, None)

                # Check if yet unseen:
                # if curr_feature_val not in self.trainDataset.feature_index_2_vals[curr_feature_index]:
                if not next_node:
                    best_clb_index = self.democracy(curr_node)  # Time for some democracy
                    # After we've done, just append the best one
                    output.append(self.trainDataset.class_labels_reverse[best_clb_index])
                    break

                if next_node.is_leaf or (self.max_depth_set and self.max_depth == curr_node.depth):
                    # 2 cases ->
                    #   a) Leaf     -- give priority to this one!
                    #   b) Hit the depth limit.
                    if next_node.is_leaf:
                        output.append(self.trainDataset.class_labels_reverse[next_node.clb_index])
                    elif self.max_depth_set and self.max_depth == curr_node.depth:
                        best_clb_index = self.democracy(next_node)          # Time for some democracy
                        # After we've done, just append the best one
                        output.append(self.trainDataset.class_labels_reverse[best_clb_index])
                    break

                # Else, we gotta go on
                curr_node = next_node

        return output

    def democracy(self, node: Node):
        # Democratic method - choose the most frequent clb
        best_clb_cnt = -1
        best_clb_index = -1

        curr_clb_cnt_index = 0
        for clb_cnt in node.clb_cnts_list:
            # Check if over half, as that would conclude
            if clb_cnt >= node.clb_cnts_sum / 2:
                # It's the one
                best_clb_index = curr_clb_cnt_index
                break

            # Proceed with the algo - bubble sorty!
            if clb_cnt > best_clb_cnt:
                best_clb_index = curr_clb_cnt_index

            curr_clb_cnt_index += 1

        return best_clb_index

    @staticmethod
    def entropy_feature_val(feature_val_clbs_cnt_list: list, clbs_cnt_sum: int):
        sigma = 0

        for feature_val_clbs_cnt in feature_val_clbs_cnt_list:
            prob = feature_val_clbs_cnt / clbs_cnt_sum
            if math.fabs(prob) <= EPSILON:
                continue

            sigma -= prob * math.log(prob, 2)

        return sigma

    @staticmethod
    def entropy_feature(feature_clbs: list, feature_clbs_sum: int):
        sigma = 0

        for feature_clb_cnt in feature_clbs:
            prob = feature_clb_cnt / feature_clbs_sum
            if math.fabs(prob) <= EPSILON:
                continue

            sigma -= prob * math.log(prob, 2)

        return sigma

    def entropy_dataset(self, D: list):
        if len(D) == 0:
            return None

        # Calculate number of class_values for each distinct class label
        class_label_quantities = {}
        for class_label in self.trainDataset.class_labels:
            class_label_quantities[class_label] = 0

        for example in D:
            class_label_quantities[example[-1]] += 1

        examples_num = len(self.trainDataset.data)
        sigma = 0

        for class_label_quantity in class_label_quantities.values():
            prob = class_label_quantity/examples_num
            if math.fabs(prob) <= EPSILON:
                continue

            sigma -= prob * math.log(prob, 2)

        return sigma

    def set_feature(self, feature_name, feature_index: int, node: Node):
        # Don't forget to set them!
        node.feature_index = feature_index
        node.feature_name = feature_name

        # Append to output!
        self.test_output.append((node.depth, feature_name))


    def entropy_specific(self, D: list, feature_index: int, feature_val: str):
        if len(D) == 0:
            return None

        sigma = 0
        # There is a single y. But it can have multiple values. Have to count occurrence of each value of y
        class_val_quantities = []
        for class_val in self.trainDataset.class_labels:
            class_val_quantities.append(0)

            # Okay, got the class_val, now count the occurrence of it in D
            for example in D:
                if example[-1] == class_val and example[feature_index] == feature_val:
                    class_val_quantities[-1] += 1

        # Okay, the class_val_quantity has now been filled
        elems_num = sum(class_val_quantities)
        for class_quantity in class_val_quantities:
            prob = class_quantity / elems_num
            sigma += prob * math.log(prob, 2)

        return -1 * sigma

    def IG(self, D: list, feature_index: int):
        # So, gotta get all the feature values of this feature --> calculate their quantities
        feature_quantities = {}
        elems_num = len(D)
        # feature_vals_counter = 0

        for example in D:
            # feature_vals_counter += 1
            key = example[feature_index]
            val = feature_quantities.get(key)

            if val is not None:
                feature_quantities[key] += 1
            else:
                feature_quantities[key] = 1

        # Got the quantities and the number of elements. Calculate and return the value now!
        sigma = self.entropy_dataset(D)
        for feature_val, feature_quantity in feature_quantities.items():
            sigma -= feature_quantity / elems_num * self.entropy_specific(D, feature_index, feature_val)

        return sigma

    def summarise_performance(self, predictions: list, testDataset: Dataset):
        # First test if a fit has been performed
        if not self.fit_done:
            raise AttributeError("The model hasn't been trained! Therefore no testing can be done!")

        # Okay, let's get started by forming a list of true outputs
        future = [row[-1] for row in testDataset.data]

        # Init the confusion matrix
        dimension = len(self.trainDataset.class_labels.keys())
        conf_matrix = []
        for i in range(dimension):
            conf_matrix.append([])
            for j in range(dimension):
                conf_matrix[i].append(0)

        elems_num = 0
        matches_num = 0
        # Okay, now gotta count the number of matches
        for pred, fut in zip(predictions, future):
            elems_num += 1
            if pred == fut:
                matches_num += 1

            pred_index = self.trainDataset.class_labels[pred]
            fut_index = self.trainDataset.class_labels[fut]

            conf_matrix[fut_index][pred_index] += 1

        acc = matches_num / elems_num
        return acc, conf_matrix
