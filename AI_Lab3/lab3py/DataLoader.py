class ConfigData:
    """
    Throw away function - decided to use a dict instead.
    """
    def __init__(self):
        self.mode = None
        self.model = None
        self.max_depth = None


class Dataset:

    def __init__(self):
        self.features = {}          # Connects a feature to its list index
        self.features_reverse = {}      # Connects a list index to a feature

        self.class_labels = {}          # Connects a class label to an index
        self.class_labels_reverse = {}          # Connects an index to a class label

        self.feature_index_2_vals = {}          # All the feature_vals of each feature(_index)
        self.feature_names_2_vals = {}          # Names version
        self.data = []                          # Examples

########################################################################################################################
########################################################################################################################
########################################################################################################################


def loadDatasetFile(path: str):
    # Okay, open the file
    train_file = open(path, 'r', encoding='utf-8')

    trainData = Dataset()
    # Let's get the first line
    line = train_file.readline().rstrip()
    line_parts = line.split(',')

    #########################################           Header parsing          ########################################
    #features_tmp = set()
    counter = 0
    for part in line_parts[0:-1]:
        trainData.features[part] = counter
        trainData.features_reverse[counter] = part
        counter += 1

    # counter = 0         # Used to enumerate features --> encode them by index
    # features_tmp_alph = sorted(features_tmp)
    # for feature in features_tmp_alph:
    #     trainData.features[feature] = counter
    #     trainData.features_reverse[counter] = feature
    #     counter += 1
    #########################################           Header parsing          ########################################

    # Prepare the dictionary!
    for feature_index in trainData.features.values():
        trainData.feature_index_2_vals[feature_index] = set()

    #########################################           Data parsing          ########################################
    # Okay, all done. Now just read the data
    class_labels_tmp = set()
    for line in train_file:
        line = line.rstrip()
        line_parts = line.split(',')

        # Update the feature values dictionary
        for feature_index in range(0, len(line_parts) - 1):
            trainData.feature_index_2_vals[feature_index].add(line_parts[feature_index])

        class_labels_tmp.add(line_parts[-1])
        trainData.data.append(line_parts)

    # Just gotta enumerate all the class labels now!
    counter = 0         # Used to enumerate class labels --> encode them by index
    # Now make a sorted list of them!
    class_labels_tmp_alph = sorted(class_labels_tmp)

    for class_label in class_labels_tmp_alph:
        trainData.class_labels[class_label] = counter
        trainData.class_labels_reverse[counter] = class_label
        counter += 1
    #########################################           Data parsing          ########################################

    # Let's make the index_2_val sets ordered alphabetically
    for index, vals in trainData.feature_index_2_vals.items():
        values_alph = sorted(vals)
        trainData.feature_index_2_vals[index] = values_alph

        feature_name = trainData.features_reverse[index]
        trainData.feature_names_2_vals[feature_name] = values_alph

    # Ta - daa done
    train_file.close()
    return trainData


def loadConfigFile(path: str):
    config_file = open(path, 'r', encoding='utf-8')

    config_data = {}

    for line in config_file:
        line = line.rstrip()
        line_parts = line.split('=')

        hyperpar_name = line_parts[0]
        hyperpar_value = line_parts[1]

        config_data[hyperpar_name] = hyperpar_value

    return config_data
