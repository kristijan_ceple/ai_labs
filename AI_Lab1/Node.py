class Node:
    def __init__(self, state:str=None, depth:int=0, cost:int=None, parent:'Node'=None, fValue:int=0):
        self.__state = state
        self.__depth = depth
        self.__parent = parent
        self.__cost = cost
        self.__fValue = fValue;

    def getState(self):
        return self.__state

    def getDepth(self):
        return self.__depth

    def getCost(self):
        return self.__cost

    def getParent(self):
        return self.__parent

    def getFValue(self):
        return self.__fValue

    # def setState(self, toSet:str):
    #     self.__state = toSet
    #
    # def setDepth(self, toSet:int):
    #     self.__depth = toSet
    #
    # def setParent(self, toSet):
    #     self.__parent = toSet
    #
    # def setCost(self, toSet:int):
    #     self.__cost = toSet
    #
    # def setFValue(self, toSet:int):
    #     self.__fValue = toSet

    def __str__(self):
        return format("(%s, %d, %d, %s)" % (self.__state, self.__depth, self.__cost, self.__parent.getState()))

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False

        # Check all other paramters
        if(self.__state != other.__state):
            return False
        elif(self.__depth != other.__depth):
            return False
        elif(self.__cost != other.__cost):
            return False
        elif(self.__parent is not other.__parent):
            return False
        elif(self.__fValue != other.__fValue):
            return False
        else:
            return True

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.__state, self.__depth, self.__cost, self.__parent, self.__fValue))