from Node import Node
from sortedcontainers import SortedKeyList
from sortedcontainers import SortedDict
from sortedcollections import ValueSortedDict
from copy import deepcopy

class DataHandler:
    """
    s0 - contains initial state
    goalStates - the set of goal states
    trans - the transitions dictionary
    """

    def __init__(self):
        self.dataLoaded = False
        self.s0 = None
        self.goalStates = set()
        self.trans = {}
        self.heuristicsDescriptor = {}
        self.dataLoaded = False
        self.heuristicsLoaded = False
        self.states = set()

    @classmethod
    def fromFilename(cls, filename):
        dh = cls()
        dh.loadData(filename)
        return dh

    def printData(self):
        if not self.dataLoaded:
            raise AttributeError("Data not yet loaded!")

        print("Initial state:\n\t%s" % self.s0)
        print("Goal states:")
        for goalState in self.goalStates:
            print("\t" + goalState)

        print("State space size: %d" % len(self.trans.keys()))

        totalTrans = 0
        for upperVals in self.trans.values():
            totalTrans += len(upperVals)
        print("Total transitions: %d" % totalTrans)

        """
        print("Transitions:")
        for (upperState, pairs) in self.trans.items():
            print("\t%s:" % upperState)
            for pair in pairs:
                print("\t\t%s, %d" % pair )
        """

        print()

    def printHeuristics(self):
        if not self.heuristicsLoaded:
            raise AttributeError("Heuristics not yet loaded!")

        print("Printing the loaded Heuristics Descriptor!")
        for state, hVal in self.heuristicsDescriptor.items():
            print("\t%s: %d" % (state, hVal))
        print()

    def loadData(self, filename):
        # Let's open the file by the name filename
        file = open(filename, "r", encoding="utf8")

        s0Loaded = False
        goalStatesLoaded = False

        # First let's read the initial state
        for line in file:
            if line.startswith("#"):
                # This line contains a comment
                continue

            if not s0Loaded:
                self.s0 = line.strip()
                s0Loaded = True
                continue

            if not goalStatesLoaded:
                for goalState in line.split(" "):
                    self.goalStates.add(goalState.strip())

                goalStatesLoaded = True
                # Now got the initial state and the goalStates loaded
                print("Initial state and goal states loaded!")
                continue

            # First split by :
            upperState, upperTrans = line.split(":")

            # Add to the set of states
            self.states.add(upperState)

            for lowerPair in upperTrans.split(" "):
                if lowerPair == '':
                    continue

                if lowerPair == '\n':
                    # No transitions, make an empty list at that position
                    self.trans[upperState] = []
                    continue

                lowerState, costStr = lowerPair.strip().split(",")
                self.trans.setdefault(upperState, []).append( (lowerState, int(costStr)) )

        print("States loading done!")
        print("Data loading done!")
        self.dataLoaded = True

    def readHeuristics(self, filename):
        hDesc = open(filename, "r", encoding="utf8")
        for line in hDesc:
            state, hVal = line.split(":")
            self.heuristicsDescriptor[state.strip()] = int(hVal.strip())

        self.heuristicsLoaded = True

    def hOptimisticsCheck(self):
        # Let's check if the heuristics is optimistic
        # That means that for every state, the heuristics descriptor must be <= the path cost to one of the final goals
        # First calc h*
        hStar = {}

        # First discover the shortest path to the goal state
        initState = self.s0
        goalStates = self.goalStates
        totalCosts = {}

        for goalState in goalStates:
            totalCosts[goalState] = DataHandler.pathCost(self, initState, goalState)
            hStar[goalState] = {}

            for state in self.states:
                # Calc the cost
                hStar[goalState][state] = DataHandler.pathCost(self, state, goalState)

        # Now we have the h* filled, time to check
        # Must hold: h(s) <= h*(s) for every state and every goal state
        optimistic = True

        for goalState in goalStates:
            for upperState, hValue in self.heuristicsDescriptor.items():
                # Let's compare
                tmp = hStar[goalState][upperState]
                if tmp is None:
                    continue        # Infinity

                if hValue > hStar[goalState][upperState]:
                    print("\t[ERR] h(%s) > h*: %d > %d" % (upperState, hValue, hStar[goalState][upperState]))
                    optimistic = False

        if optimistic:
            print("Heuristic is optimistic!\n")
        else:
            print("Heuristic is not optimistic!\n")

    def hConsistencyCheck(self):
        consistent = True
         # Let's start from the initialState
        openList = [self.s0]
        visitedStates = set()
        while len(openList) > 0:
            node = openList[0]
            openList.pop(0)

            if node in visitedStates:
                continue       # Already did this state

            for neighbourState, neighbourCost in self.trans[node]:
                openList.append(neighbourState)

                if self.heuristicsDescriptor[node] > ( self.heuristicsDescriptor[neighbourState] + neighbourCost ):
                    # Not consistent
                    consistent = False
                    print("\t[ERR] h(%s) > h(%s) + c: %d > %d + %d" % (node, neighbourState,
                                                                       self.heuristicsDescriptor[node],
                                                                       self.heuristicsDescriptor[neighbourState],
                                                                       neighbourCost))

            # Just passed all the neighbour states
            # All the neighbours are in the openList -- continue the while loop
            visitedStates.add(node)

        # While loop done - print the final report
        if consistent:
            print("Heuristic is consistent!")
        else:
            print("Heuristic is not consistent!")

    def hOptimisticsCheckOptimisedDijkstra(self):
        print("Beginning Dijkstra...")
        hStar = {}

        # Let's prepare a reverse Transitions
        reverseTrans = {}

        # Go around trans now
        for upperState, lowerTrans in self.trans.items():
            # Have got the state and its transitions now
            for lowerState, lowerCost in lowerTrans:
                # Excellent, add an entry to reverseTrans
                reverseTrans.setdefault(lowerState, default=[]).append((upperState, lowerCost))

        print("Constructed reverseTrans")
        # The reverseTrans has now been constructed - let's construct h*
        for goalState in self.goalStates:
            # For every goal state check optimism! Put the best result into h*

            # closed = []
            openList = SortedKeyList(key=lambda argTup: argTup[1])
            visitedStates = set()

            initTuple = (goalState, 0)
            openList.add(initTuple)

            while len(openList) > 0:
                currTuple = openList.pop(0)
                state = currTuple[0]

                if state in visitedStates:
                    continue

                visitedStates.add(state)
                g = currTuple[1]
                iterateOver = reverseTrans.get(state)

                if iterateOver is not None:
                    for neighbourState, cost in iterateOver:
                        toAppend = (neighbourState, g + cost)
                        openList.add(toAppend)

                # This state and its neighbours have then been processed
                # It is time to add the value of g to h*
                # If g is greater than current value then put it instead in h*
                formerG = hStar.setdefault(state, g)
                if formerG is not None and g < formerG:
                    # Something was inside, and that something is greater than our current g
                    hStar[state] = g

        # h* has been constructed, but this is an inversed h* lmao
        # Still holds heh

        print("Testing the equation...")
        optimistic = True
        for state in self.states:
            # Let's turn the path from every state tothe goal state
            # h(s) <= h*(s)
            hVal = self.heuristicsDescriptor[state]
            hStarVal = hStar.get(state)
            if hStarVal is None:
                # Infinity, prolly optimistic
                continue

            # Check if the equation holds
            if hVal > hStarVal:
                print("\t[ERR] h(%s) > h*: %d > %d" % (state, hVal, hStarVal))
                optimistic = False

        if optimistic:
            print("Heuristic is optimistic!")
        else:
            print("Heuristic is not optimistic!")

    def hOptimisticsCheckOptimised(self):
        hStar = {}

        # Let's get the initialState
        # Let's find the path from the initialState to goal State
        for goalState in self.goalStates:
            # Whatever - for now works
            hStar[goalState] = {}
            statesLeft = ValueSortedDict(self.heuristicsDescriptor)

            while len(statesLeft) > 0:
                nextStateTuple = statesLeft.popitem()
                openList = SortedKeyList(key=lambda tuple: tuple[1])
                openList.add((nextStateTuple[0], 0, None))
                visitedStates = set()
                tracebackTuple = None
                currTuple = None

                while len(openList) > 0:
                    currTuple = openList.pop(0)
                    state = currTuple[0]

                    if state in visitedStates:
                        continue  # Have already been here
                    elif state == goalState:
                        # Found the shortest path
                        tracebackTuple = currTuple
                        break
                    visitedStates.add(state)

                    g = currTuple[1]
                    for neighbourState, lowerCost in self.trans[state]:
                        toAdd = (neighbourState, g + lowerCost, currTuple)
                        openList.add(toAdd)

                # Now, for the current state(initially initial state s0 :P) and the current goal state, I have the shortest path
                # Need to traceback and update hStar, if a path was found. Otherwise, just skip
                if tracebackTuple is not None:
                    self.tracebackOracle(tracebackTuple, hStar, goalState, statesLeft)

                # Now, we have to check if there are any states which haven't been covered in this traceback
                # Get them, and get the one with the lowest h, rerun this loop

        # Nooow we have hStar filled - do the check
        optimistic = True
        for goalState in self.goalStates:
            for state in self.states:
                # Check!
                compareTo = hStar[goalState].get(state)
                if compareTo is None:
                    continue        # Surely, every number is less than infinity

                if self.heuristicsDescriptor[state] > compareTo:
                    optimistic = False
                    print("\t[ERR] h(%s) > h*: %d > %d" % (state, self.heuristicsDescriptor[state], compareTo))

        if optimistic:
            print("Heuristic is optimistic!")
        else:
            print("Heuristic is not optimistic!")

    def tracebackOracle(self, tracebackTuple:(), hStar:{}, goalState:str, statesLeft:SortedDict):
        # First need to get the prices ordered accordingly
        tmpTuple = tracebackTuple
        totalPathCost = tracebackTuple[1]
        while tracebackTuple is not None:
            state = tracebackTuple[0]
            cost = tracebackTuple[1]
            parent = tracebackTuple[2]
            toSet = totalPathCost - cost

            tmp = hStar[goalState].setdefault(state, default=toSet)
            if tmp is not None and toSet < tmp:
                hStar[goalState][state] = toSet

            # coveredStates.append(tmpTuple[0])
            statesLeft.pop(state, None)
            tracebackTuple = parent
        # That should be it?
        # return coveredStates

    def heuristicsCheck(self):
        if not self.heuristicsLoaded:
            raise AttributeError("Error! Heuristics not loaded!")

        print("Checking heuristics")
        print("Checking if heuristic is optimistic.")
        self.hOptimisticsCheckOptimisedDijkstra()
        print("Checking if heuristic is consistent.")
        self.hConsistencyCheck()
        print()

    @staticmethod
    def pathCost(dh: 'DataHandler', fromState: str, toState: str = None):
        # print("Calculating path cost...")
        # print("Calculating cost from %s to %s..." % (fromState, toState))

        # First check if data is loaded
        if not dh.dataLoaded:
            raise ValueError("The data is not loaded")

        openList = SortedKeyList(key=lambda tuple: tuple[1])
        visitedStates = set()

        # Make the beginning node and add it
        initNode = (fromState, 0)
        openList.add(initNode)

        goalNode = None
        while len(openList) > 0:
            # Remove Head (from the slides)
            node = openList.pop(0)
            state = node[0]
            if state in visitedStates:
                continue
            g = node[1]

            # Mark the state as visited
            visitedStates.add(state)

            # Check if goal state
            if toState is not None and state == toState:
                goalNode = node
                break
            elif toState is None:
                # Can be none, or can be different from toState
                if state in dh.goalStates:
                    goalNode = node
                    break

            # Get the successors -- expansion
            for neighbourPair in dh.trans[state]:
                neighbourState, neighbourCost = neighbourPair
                toAppend = (neighbourState, g + neighbourCost)

                # Insert into the list
                openList.add(toAppend)

            # Continue the loop now - but first sort the list!

        # Return the path cost
        if goalNode is None:
            None
        else:
            return goalNode[1]