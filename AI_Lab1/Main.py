from DataHandler import DataHandler
import Algorithms

# aiDh = DataHandler.fromFilename("ai.txt")
# aiDh.printData()
# Algorithms.BFS(aiDh)
# Algorithms.UCS(aiDh)
#
# aiDh.readHeuristics("ai_fail.txt")
# aiDh.printHeuristics()
# Algorithms.AStar(aiDh)
# aiDh.heuristicsCheck()
#
# aiDh.readHeuristics("ai_pass.txt")
# aiDh.printHeuristics()
# Algorithms.AStar(aiDh)
# aiDh.heuristicsCheck()

istraDh = DataHandler.fromFilename("istra.txt")
istraDh.printData()
#
# Algorithms.BFS(istraDh)
# Algorithms.UCS(istraDh)

istraDh.readHeuristics("istra_heuristic.txt")
istraDh.printHeuristics()
Algorithms.AStarOptimised(istraDh)
# istraDh.heuristicsCheck()

# istraDh.readHeuristics("istra_pessimistic_heuristic.txt")
# istraDh.printHeuristics()
# Algorithms.AStar(istraDh)
# istraDh.heuristicsCheck()
#
# eightPuzzleDh = DataHandler.fromFilename("3x3_puzzle.txt")
# eightPuzzleDh.printData()
# eightPuzzleDh.readHeuristics("3x3_misplaced_heuristic.txt")
# eightPuzzleDh.printHeuristics()
# Algorithms.AStarOptimised(eightPuzzleDh)
# eightPuzzleDh.heuristicsCheck()
# Algorithms.BFS(eightPuzzleDh)
# Algorithms.UCS(eightPuzzleDh)
# Algorithms.AStarOptimised(eightPuzzleDh)
# Algorithms.AStar(eightPuzzleDh)       Doesn't finish under 5 mins :(