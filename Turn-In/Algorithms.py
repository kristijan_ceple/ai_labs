from sortedcontainers import SortedKeyList, SortedDict, SortedList
from Node import Node
from DataHandler import DataHandler


def traceback(currNode: Node):
    # Stop if we've reached the top
    if currNode.getParent() is None:
        print("%s =>" % currNode.getState())
        return

    traceback(currNode.getParent())
    print("%s =>" % currNode.getState())


def AStar(dh: DataHandler):
    print("Running A*(non-optimised):")

    # First check if data is loaded
    if not dh.dataLoaded or not dh.heuristicsLoaded:
        raise ValueError("The data or the heuristics not loaded")

    openList = SortedKeyList(key=lambda nodePar: nodePar.getFValue())
    closed = set()
    visited_states_dict = {}
    visited_states_set = set()

    # Make the beginning node and add it
    initNode = Node(dh.s0, 0, 0, None, dh.heuristicsDescriptor[dh.s0])
    openList.add(initNode)

    # Let's go from the initial state, disregard distance, just search for the goal state
    goalNode = None

    while len(openList) > 0:
        # Remove Head (from the slides_
        node = openList.pop(0)
        depth = node.getDepth()
        g = node.getCost()
        state = node.getState()

        # Mark the state as visited
        closed.add(node)
        visited_states_dict[state] = (node, closed)       # Mark as closed
        visited_states_set.add(state)

        # Check if goal state
        if state in dh.goalStates:
            goalNode = node
            break

        # Get the successors -- expansion
        for neighbourPair in dh.trans[state]:
            neighbourState, neighbourCost = neighbourPair

            lowerG = g + neighbourCost
            fValue = lowerG + dh.heuristicsDescriptor[neighbourState]

            if neighbourState in visited_states_dict:
                # We've found it!
                foundNode, foundWhere = visited_states_dict[neighbourState]
                if foundNode.getCost() <= neighbourCost:
                    continue
                else:
                    # Remove the foundNode from the corresponding
                    if foundWhere == openList:
                        foundPrice = foundNode.getCost()

                    else:
                        closed.remove(foundNode)

            toAdd = Node(neighbourState, depth + 1, lowerG, node, fValue)

            # Insert into the list
            openList.add(toAdd)
            visited_states_dict[neighbourState] = (toAdd, openList)               # Immediately update the dictionary!

        # Continue the loop now - but first sort the list!

    # Let's traceback and write other info
    print("States visited = %d" % len(visited_states_set))
    print("Number nodes in the closed list: %d" % len(closed))
    print("Found path of length %d with total cost %d" % (goalNode.getDepth() + 1, goalNode.getCost()))
    traceback(goalNode)
    print()


def AStarOptimised(dh: DataHandler):

    print("Running A* Optimised Edition:")

    # First check if data is loaded
    if not dh.dataLoaded or not dh.heuristicsLoaded:
        raise ValueError("The data or the heuristics not loaded")

    openList = SortedKeyList(key=lambda nodePar: nodePar.getFValue())
    # closed = []
    visitedStates = set()

    # Make the beginning node and add it
    initNode = Node(dh.s0, 0, 0, None, dh.heuristicsDescriptor[dh.s0])
    openList.add(initNode)

    # Let's go from the initial state, disregard distance, just search for the goal state
    goalNode = None

    while len(openList) > 0:
        # Remove Head (from the slides_
        node = openList.pop(0)
        depth = node.getDepth()
        g = node.getCost()
        state = node.getState()

        if state in visitedStates:
            continue

        # Mark the state as visited
        # closed.append(node)
        visitedStates.add(state)

        # Check if goal state
        if state in dh.goalStates:
            goalNode = node
            break

        # Get the successors -- expansion
        for neighbourPair in dh.trans[state]:
            neighbourState, neighbourCost = neighbourPair

            lowerG = g + neighbourCost
            fValue = max(node.getFValue(), lowerG + dh.heuristicsDescriptor[neighbourState])

            toAdd = Node(neighbourState, depth + 1, lowerG, node, fValue)

            # Insert into the list
            openList.add(toAdd)

        # Continue the loop now - but first sort the list!

    # Let's traceback and write other info
    print("States visited = %d" % len(visitedStates))
    # print("Number nodes in the closed list: %d" % len(closed))
    print("Found path of length %d with total cost %d" % (goalNode.getDepth() + 1, goalNode.getCost()))
    traceback(goalNode)
    print()


def UCS(dh: DataHandler):
    print("Running UCS:")

    # First check if data is loaded
    if not dh.dataLoaded:
        raise ValueError("The data is not loaded")

    openList = SortedKeyList(key=lambda nodePar: nodePar.getCost())
    # closed = []
    visitedStates = set()

    # Make the beginning node and add it
    initNode = Node(dh.s0, 0, 0, None)
    openList.add(initNode)

    # Let's go from the initial state, disregard distance, just search for the goal state
    goalNode = None

    while len(openList) > 0:
        # Remove Head (from the slides_
        node = openList.pop(0)
        depth = node.getDepth()
        g = node.getCost()
        state = node.getState()

        if state in visitedStates:
            continue

        # Mark the state as visited
        # closed.append(node)
        visitedStates.add(state)

        # Check if goal state
        if state in dh.goalStates:
            goalNode = node
            break

        # Get the successors -- expansion
        for neighbourPair in dh.trans[state]:
            neighbourState, neighbourCost = neighbourPair
            toAppend = Node(neighbourState, depth + 1, g + neighbourCost, node)

            # Insert into the list
            openList.add(toAppend)

        # Continue the loop now - but first sort the list!

    # Let's traceback and write other info
    print("States visited = %d" % len(visitedStates))
    # print("Number nodes in the closed list: %d" % len(closed))
    print("Found path of length %d with total cost %d" % (goalNode.getDepth() + 1, goalNode.getCost()))
    traceback(goalNode)
    print()


def BFS(dh: DataHandler):
    print("Running BFS:")

    # First check if data is loaded
    if not dh.dataLoaded:
        raise ValueError("The data is not loaded")

    openList = []
    # closed = []
    visitedStates = set()

    # Make the beginning node and add it
    initNode = Node(dh.s0, 0, 0, None)
    openList.append(initNode)

    # Let's go from the initial state, disregard distance, just search for the goal state
    goalNode = None

    while len(openList) > 0:
        # Remove Head (from the slides_
        node = openList.pop(0)
        depth = node.getDepth()
        g = node.getCost()
        state = node.getState()

        if state in visitedStates:
            continue

        # Mark the state as visited
        # closed.append(node)
        visitedStates.add(state)

        # Check if goal state
        if state in dh.goalStates:
            goalNode = node
            break

        # Get the successors -- expansion
        for neighbourPair in dh.trans[state]:
            neighbourState, neighbourCost = neighbourPair
            toAppend = Node(neighbourState, depth + 1, g + neighbourCost, node)
            openList.append(toAppend)

        # Continue the loop now

    # Let's traceback and write other info
    print("States visited = %d" % len(visitedStates))
    # print("Number nodes in the closed list: %d" % len(closed))
    print("Found path of length %d with total cost %d" % (goalNode.getDepth() + 1, goalNode.getCost()))
    traceback(goalNode)
    print()
