import time
from DataHandler import DataHandler
import Algorithms


eightPuzzleDh = DataHandler.fromFilename("3x3_puzzle.txt")
eightPuzzleDh.readHeuristics("3x3_misplaced_heuristic.txt")

tstart = time.time()
for i in range(100):
    Algorithms.AStar(eightPuzzleDh)
tend = time.time()

print("Time required: %.2f" % (tend - tstart))